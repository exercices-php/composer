# Exercices Composer

## Installation

Lancer la commande qui permet de recréer le répertoire `vendor` à partir du fichier `composer.lock`

## Scripts

Vous pouvez lancer la démo en tapant la commande :

```
php app/demo.php
```

La démo doit normalement afficher :

```
[info] hello called
[info] hello called
[info] hello called
```

Créer un script `demo` dans le fichier `composer.json`

Lancer la commande :

```
composer run demo
```

## Mise à jour

Metter à jour composer via la commande adéquat

Utiliser la commande `composer outdated` pour connaitre les dépendances qui nécessitent une mise à jour.

Mettez à jour `psr/log` avec la commande `composer update`

Modifiez le fichier `composer.json` pour que `psr/log` s'installe dans sa version 1 la plus récente

Mettez à jour `psr/log` avec la commande `composer update`

## Platform checks

Dans le fichier `composer.json`, ajouter dans la section `require` la dépendance suivante :

```
"php": "5.4.*",
```

Lancer ensuite la commande `composer check-platform-reqs` pour vérifier si votre platforme PHP est compatible avec celle indiquée dans le fichier.

Modifier la version de PHP dans le fichier `composer.json` pour que composer accepte :
- n'importe quelle version de PHP 5 supérieure ou égale à 5.4
- n'importe quelle version de PHP 7
- aucune autre version (par exemple pas PHP 8)

## Autoloader

Dans le fichier `composer.json` ajouter une section `autoload` avec la config `PSR-4` qui fait correspondre le namespace `FormationTech` au répertoire `lib`

Lancer ensuite la commande `composer dump-autoload`

Dans le fichier `app/demo.php` retirer la ligne 

```php
require_once 'lib/Logger/EchoLogger.php';
```

Et vérifier que tout fonctionne comme avant.

## Patches

Installez avec composer le paquet : `cweagans/composer-patches`

En vous inspirant de la doc sur [https://github.com/cweagans/composer-patches]() appliquer le patch `patches/psr-log-warn.patch` à la lib `psr/log`

Ce patch va ajouter une méthode `warn` aux classes de type `Logger`

Supprimez le vendor et réinstallez-le.

Vérifier dans `app/demo.php` que la méthode `warn` existe sur l'objet `$logger`.

