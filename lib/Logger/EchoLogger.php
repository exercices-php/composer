<?php

namespace FormationTech\Logger;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

class EchoLogger implements LoggerInterface {
    use LoggerTrait;

    public function log($level, $message, array $context = array())
    {
        echo "[$level] $message\n";
    }
}