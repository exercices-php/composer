<?php

require_once 'vendor/autoload.php';
require_once 'lib/Logger/EchoLogger.php';

function hello() {
    $logger = new \FormationTech\Logger\EchoLogger();
    $logger->info('hello called');
}

hello();
hello();
hello();
